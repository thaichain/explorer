module github.com/gochain-io/explorer

go 1.13

require (
	github.com/aristanetworks/goarista v0.0.0-20181109020153-5faa74ffbed7 // indirect
	github.com/blendle/zapdriver v1.1.6
	github.com/btcsuite/btcd v0.0.0-20181013004428-67e573d211ac // indirect
	github.com/cespare/cp v1.1.1 // indirect
	github.com/cespare/xxhash v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v4.0.1+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gochain-io/gochain/v3 v3.1.3
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/gorilla/schema v1.1.0
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/huin/goupnp v1.0.0 // indirect
	github.com/karalabe/hid v0.0.0-20181128192157-d815e0c1a2e2 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/minio/minio-go v6.0.14+incompatible // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	github.com/rs/cors v1.6.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
	github.com/smartystreets/goconvey v0.0.0-20190222223459-a17d461953aa // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/syndtr/goleveldb v0.0.0-20181105012736-f9080354173f // indirect
	github.com/urfave/cli v1.20.0
	go.opencensus.io v0.18.0 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
